﻿namespace Ravenclaw.Backend.Constants;

public static class FigureValues
{
    public const int None = 0;
    public const int Fog = 1;
    public const int Pawn = 5;
    public const int Knight = 7;
    public const int Bishop = 9;
    public const int Rook = 10;
    public const int Queen = 50;
    public const int King = 100;


    // x y

    public static List<int[]> TopSteps = new()
    {
        new int[] { 0, -1 },
        new int[] { 0, -2 },
        new int[] { 0, -3 },
        new int[] { 0, -4 },
        new int[] { 0, -5 },
        new int[] { 0, -6 },
        new int[] { 0, -7 },
    };
    public static List<int[]> DownSteps = new()
    {
        new int[] { 0, 1 },
        new int[] { 0, 2 },
        new int[] { 0, 3 },
        new int[] { 0, 4 },
        new int[] { 0, 5 },
        new int[] { 0, 6 },
        new int[] { 0, 7 },
    };
    public static List<int[]> LeftSteps = new()
    {
        new int[] { -1, 0 },
        new int[] { -2, 0 },
        new int[] { -3, 0 },
        new int[] { -4, 0 },
        new int[] { -5, 0 },
        new int[] { -6, 0 },
        new int[] { -7, 0 },
    };
    public static List<int[]> RightSteps = new()
    {
        new int[] { 1, 0 },
        new int[] { 2, 0 },
        new int[] { 3, 0 },
        new int[] { 4, 0 },
        new int[] { 5, 0 },
        new int[] { 6, 0 },
        new int[] { 7, 0 },
    };
 


    /// ///////////////////
    public static List<int[]> TopLeftSteps = new()
    {
        new int[] { -1, -1 },
        new int[] { -2, -2 },
        new int[] { -3, -3 },
        new int[] { -4, -4 },
        new int[] { -5, -5 },
        new int[] { -6, -6 },
        new int[] { -7, -7 },
    };
    public static List<int[]> TopRightSteps = new()
    {
        new int[] { 1, -1 },
        new int[] { 2, -2 },
        new int[] { 3, -3 },
        new int[] { 4, -4 },
        new int[] { 5, -5 },
        new int[] { 6, -6 },
        new int[] { 7, -7 },
    };

    public static List<int[]> DownRightSteps = new()
    {
        new int[] { 1, 1 },
        new int[] { 2, 2 },
        new int[] { 3, 3 },
        new int[] { 4, 4 },
        new int[] { 5, 5 },
        new int[] { 6, 6 },
        new int[] { 7, 7 },
    };
    public static List<int[]> DownLeftSteps = new()
    {
        new int[] { -1, 1 },
        new int[] { -2, 2 },
        new int[] { -3, 3 },
        new int[] { -4, 4 },
        new int[] { -5, 5 },
        new int[] { -6, 6 },
        new int[] { -7, 7 },
    };
    ////////////////////////
    public static List<int[]> KingSteps = new()
    {
        new int[] { 0, 1 },
        new int[] { 1, 1 },
        new int[] { 1, 0 },
        new int[] { 1, -1 },
        new int[] { 0, -1 },
        new int[] { -1, -1 },
        new int[] { -1, 0 },
        new int[] { -1, 1 },
    };
    public static List<int[]> KnightSteps = new()
    {
        new int[] { 1, 2 },
        new int[] { 2, 1 },
        new int[] { 2, -1 },
        new int[] { 1, -2 },
        new int[] { -1, -2 },
        new int[] { -2, -1 },
        new int[] { -2, 1 },
        new int[] { -1, 2 },
    };
}