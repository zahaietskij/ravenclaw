﻿namespace Ravenclaw.Backend.Constants;

public enum Colors
{
    None = 0,
    Black = 1,
    White = 2,
    Any = 3
}