﻿using Ravenclaw.Backend.Constants;
using Ravenclaw.Backend.Models;

namespace Ravenclaw.Backend.ChessValidator
{

    public static class ChessValidator
    {
        public static bool StepValidator(Game game, Step step)
        {
            int[][] boardStart = game.Board.Figures;
            Cage startCage = new Cage() { };
            Cage endCage = new Cage() { };
            var intStep = StringStepToIntStep(step);

            var a = game.Board.Figures[8 - (step.From[0] - '0')][step.From[1] - 'a'];

            ChessStepValidator stepValid = new ChessStepValidator();
            if (boardStart.Length != 0)
            { 

                startCage = new Cage() { Figure = boardStart[intStep.From[1]][intStep.From[0]], CagePosition = new int[] { intStep.From[0], intStep.From[1] } };
                endCage = new Cage() { Figure = boardStart[intStep.To[1]][intStep.To[0]], CagePosition = new int[] { intStep.To[0], intStep.To[1] } };
                return StepIsTrue(endCage, startCage, boardStart);
            }
            else
            {
                return false;
            }
        }
        private static bool StepIsTrue(Cage endCage, Cage startCage, int[][] startBoard)
        {
            //"перша клітинка" - клітинка з якої був зроблений хід, "друга клітинка" - клітинка на яку був зроблений хід

            //фігура на першій клітинці
            var startCageFigure = startCage.Figure;

            //фігура на другій клітинці
            var endCageFigure = endCage.Figure;

            ChessStepValidator step = new ChessStepValidator { From = startCage.CagePosition, To = endCage.CagePosition };

            

            //Х першої клітинки
            var x1 = startCage.CagePosition[0];
            //У першої клітинки
            var y1 = startCage.CagePosition[1];

            //Х другої клітинки
            var x2 = endCage.CagePosition[0];
            //У другої клітинки
            var y2 = endCage.CagePosition[1];

            //різниця по Х
            var deltaX = Math.Abs(x1 - x2);
            //різниця по У
            var deltaY = Math.Abs(y1 - y2);

            if ((x1 >= 0 && x2 >= 0 && y1 >= 0 && y2 >= 0) && (x1 < 8 && x2 < 8 && y1 < 8 && y2 < 8))
            {

                var whiteFigure = startCageFigure > 0;
                var blackFigure = startCageFigure < 0;
                var emptyRoad = false;
                var standartMove = false;
                var move = false;
                var fight = false;
                //пешка
                if (startCageFigure == FigureValues.Pawn || startCageFigure == -FigureValues.Pawn)
                {
                    emptyRoad = EmptyRoadCheck(startCageFigure, step, startBoard);
                    var moveToBack = (y2 > y1 && whiteFigure) || (y2 < y1 && blackFigure);
                    move = deltaX == 0 && deltaY == 1 && endCageFigure == FigureValues.None; // стандартний хід
                    standartMove = deltaX == 0 && (deltaY == 2 || deltaY == 1) && endCageFigure == FigureValues.None &&
                                    ((blackFigure && (y1 == 1)) || (whiteFigure && (y1 == 6))) && emptyRoad; // перший хід
                    fight = deltaX == 1 && deltaY == 1 && endCageFigure != FigureValues.None && BattleConditionColor(startCageFigure, endCageFigure); // бій
                    if (moveToBack)
                    {
                        return false; 
                    }
                    if (move || standartMove || fight)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                if (startCageFigure == FigureValues.Knight || startCageFigure == -FigureValues.Knight)//Кінь буква "Г"
                {
                    standartMove = (deltaX == 2 && deltaY == 1) || (deltaX == 1 && deltaY == 2);
                    move = standartMove && endCageFigure == FigureValues.None;
                    fight = standartMove && BattleConditionColor(startCageFigure, endCageFigure);

                    if (move || fight)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                if (startCageFigure == FigureValues.Bishop || startCageFigure == -FigureValues.Bishop)//Слон діагональ
                {   
                    standartMove = deltaY == deltaX;
                    emptyRoad = EmptyRoadCheck(FigureValues.Bishop, step, startBoard);
                    move = standartMove && emptyRoad && endCageFigure == FigureValues.None;
                    fight = standartMove && emptyRoad && BattleConditionColor(startCageFigure, endCageFigure);
                    if (move || fight)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                if (startCageFigure == FigureValues.Rook || startCageFigure == -FigureValues.Rook)//Ладья по прямих
                {
                    standartMove = (deltaX == 0 && deltaY > 0) || (deltaX > 0 && deltaY == 0);
                    emptyRoad = EmptyRoadCheck(FigureValues.Rook, step, startBoard);
                    move = standartMove && endCageFigure == FigureValues.None && emptyRoad;
                    fight = standartMove && BattleConditionColor(startCageFigure, endCageFigure) && emptyRoad;
                    if (move || fight)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                if (startCageFigure == FigureValues.Queen || startCageFigure == -FigureValues.Queen)//Королева діагональ + по прямих
                {
                    standartMove = (deltaX == 0 && deltaY > 0) || (deltaX > 0 && deltaY == 0) || (deltaY == deltaX);
                    emptyRoad = EmptyRoadCheck(FigureValues.Queen, step, startBoard);
                    move = standartMove && emptyRoad && endCageFigure == FigureValues.None;
                    fight = standartMove && emptyRoad && BattleConditionColor(startCageFigure, endCageFigure);
                    if (move || fight)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                if (startCageFigure == FigureValues.King || startCageFigure == -FigureValues.King)//Король (діагональ + прямі) на одну клітинку
                {
                    var safeStep = safeKingStep(endCage, startCage, startBoard);
                    standartMove = (deltaX == 1) && (deltaY == 1) || (deltaX == 0) && (deltaY == 1) || (deltaX == 1) && (deltaY == 0);
                    move = standartMove && endCageFigure == FigureValues.None && safeStep;
                    fight = standartMove && BattleConditionColor(startCageFigure, endCageFigure) && safeStep;
                    if (move || fight)
                    {
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }

            return false;
        }

        public static bool BattleConditionColor(int figure1, int figure2)
        {
            var mult = figure1 * figure2;

            if (mult < 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool EmptyRoadCheck(int figure, ChessStepValidator step, int[][] startBoard)
        {
            var x1 = step.From[0];
            var x2 = step.To[0];

            var y1 = step.From[1];
            var y2 = step.To[1];

            var deltaX = Math.Abs(x1 - x2);
            var deltaY = Math.Abs(y1 - y2);

            if ((deltaX == 1 && deltaY == 1) || (deltaX == 0 && deltaY == 1) || (deltaX == 1 && deltaY == 0))
            {
                return true;
            }
            var stopCoordinate = 0;
            var startBoardCageValue = 0;

            if (figure == FigureValues.Pawn || figure == -FigureValues.Pawn) 
            {
                if (figure == FigureValues.Pawn)
                {
                    y1--;
                }
                if (figure == -FigureValues.Pawn)
                {
                    y1++;  
                }
                if (startBoard[y1][x1] == FigureValues.None)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            if ((figure == FigureValues.Bishop) || (figure == -FigureValues.Bishop))
            {
                if ((x1 < x2) && (y1 < y2))
                {
                    stopCoordinate = x2 - 1;
                    x1++;
                    y1++;

                    for (int i = x1; i <= stopCoordinate; i++)
                    {
                        startBoardCageValue = startBoard[y1][x1];
                        if (startBoardCageValue != FigureValues.None) { return false; }
                        if (i >= 8) { return false; }
                        x1++;
                        y1++;
                    }
                    return true;

                }
                else if ((x1 > x2) && (y1 > y2))
                {
                    stopCoordinate = x2 + 1;
                    x1--;
                    y1--;

                    for (int i = x1; i >= stopCoordinate; i--)
                    {
                        startBoardCageValue = startBoard[y1][x1];
                        if (startBoardCageValue != FigureValues.None) { return false; }
                        if (i <= -1) { return false; }
                        x1--;
                        y1--;
                    }
                    return true;

                }
                else if ((x1 > x2) && (y1 < y2))
                {
                    stopCoordinate = x2 + 1;
                    x1--;
                    y1++;

                    for (int i = x1; i >= stopCoordinate; i--)
                    {

                        startBoardCageValue = startBoard[y1][x1];
                        if (startBoardCageValue != FigureValues.None) { return false; }
                        if (i <= -1) { return false; }
                        x1--;
                        y1++;
                    }
                    return true;
                }
                else if ((x1 < x2) && (y1 > y2))
                {
                    stopCoordinate = x2 - 1;
                    x1++;
                    y1--;

                    for (int i = x1; i <= stopCoordinate; i++)
                    {

                        startBoardCageValue = startBoard[y1][x1];
                        if (startBoardCageValue != FigureValues.None) { return false; }
                        if (i >= 8) { return false; }
                        x1++;
                        y1--;
                    }
                    return true;
                }
                else return false;
            }
            else if (figure == FigureValues.Rook)
            {
                if (deltaY == 0 && deltaX != 0)
                {
                    if (x1 < x2)
                    {
                        stopCoordinate = x2 - 1;
                        x1++;
                        for (int i = x1; i <= stopCoordinate; i++)
                        {

                            startBoardCageValue = startBoard[y1][x1];

                            if (startBoardCageValue != FigureValues.None) { return false; }
                            if (i >= 8) { return false; }
                            x1++;
                        }
                        return true;
                    }
                    else if (x1 > x2)
                    {
                        stopCoordinate = x2 + 1;
                        x1--;
                        for (int i = x1; i >= stopCoordinate; i--)
                        {

                            startBoardCageValue = startBoard[y1][x1];

                            if (startBoardCageValue != FigureValues.None) { return false; }
                            if (i <= -1) { return false; }
                            x1--;
                        }
                        return true;

                    }
                    else return false;
                }       
                else if (deltaX == 0 && deltaY != 0)
                {
                    if (y1 < y2)
                    {
                        stopCoordinate = y2 - 1;
                        y1++;

                        for (int i = y1; i <= stopCoordinate; i++)
                        {
                            startBoardCageValue = startBoard[y1][x1];

                            if (startBoardCageValue != FigureValues.None) { return false; }
                            if (i >= 8) { return false; }
                            y1++;
                        }
                        return true;
                    }
                    else if (y1 > y2)
                    {
                        stopCoordinate = y2 + 1;
                        y1--;
                        for (int i = y1; i >= stopCoordinate; i--)
                        {

                            startBoardCageValue = startBoard[y1][x1];
                            if (startBoardCageValue != FigureValues.None) { return false; }
                            if (i <= -1) { return false; }
                            y1--;
                        }
                        return true;

                    }
                    else return false;


                }
                else return false;

            }
            else if (figure == FigureValues.Queen)
            {
                if (deltaX == deltaY)
                {
                    return EmptyRoadCheck(FigureValues.Bishop, step, startBoard);
                }
                if ((deltaX == 0 && deltaY != 0) || (deltaX != 0 && deltaY == 0))
                {
                    return EmptyRoadCheck(FigureValues.Rook, step, startBoard);
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }


        }

        public static bool safeKingStep(Cage endCage, Cage startCage, int[][] startBoard)//перевірити всі фігури протилежного кольору 
        {
            var figure = startCage.Figure;
            var color = Colors.None;

            if (figure > 0) { color = Colors.White; }
            if (figure < 0) { color = Colors.Black; }

            var figures = GetFigures(startBoard, color);

            for (int i = 0; i < figures.Count; i++)
            {
                var figureValue = figures[i];
                
                startCage = new Cage() { Figure = figureValue.Figure, CagePosition = figureValue.CagePosition };

                if (figureValue.Figure == FigureValues.Pawn || figureValue.Figure == -FigureValues.Pawn)
                {
                    if (PawnFight(endCage, startCage))
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }
                else if (figureValue.Figure == FigureValues.King || figureValue.Figure == -FigureValues.King)
                {
                    if (KingFight(endCage, startCage))
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }
                if (StepIsTrue(endCage, startCage, startBoard)) { return false; }
            }
            return true;

        }

        private static ChessStepValidator StringStepToIntStep(Step step)
        {
            int[] from = new int[] { step.From[1] - 'a', 8 - (step.From[0] - '0')}; //x, y
            int[] to = new int[] { step.To[1] - 'a', 8 - (step.To[0] - '0') };// x, y
   
            return new ChessStepValidator() { From = from, To = to };

        }

        public static List<Cage> GetFigures(int[][] startBoart, Colors kingColor)
        {
            List<Cage> figures = new List<Cage>();
            if (kingColor == Colors.Black)
            {
                for (int i = 0; i < startBoart.Length; i++)
                {

                    for (int j = 0; j < startBoart[i].Length; j++)
                    {
                        var figure = startBoart[i][j];

                        if (figure >= 5)
                        {
                            figures.Add(new Cage() { Figure = figure, CagePosition = new int[] { j, i } });
                        }
                    }

                }
            }

            if (kingColor == Colors.White)
            {
                for (int i = 0; i < startBoart.Length; i++)
                {

                    for (int j = 0; j < startBoart[i].Length; j++)
                    {
                        var figure = startBoart[i][j];
                        if (figure <= -5)
                        {
                            figures.Add(new Cage() { Figure = figure, CagePosition = new int[] { j, i } });
                        }
                    }

                }
            }
            return new List<Cage>(figures);
        }
        static bool PawnFight(Cage endCage, Cage startCage)
        {
            var x1 = startCage.CagePosition[0];
            var y1 = startCage.CagePosition[1];

            var x2 = endCage.CagePosition[0];
            var y2 = endCage.CagePosition[1];

            var deltaX = Math.Abs(x1 - x2);
            var deltaY = Math.Abs(y1 - y2);

            var startCageFigure = startCage.Figure;
            var endCageFigure = endCage.Figure;

            var whiteFigure = startCageFigure > 0;
            var blackFigure = startCageFigure < 0;

            var result = deltaX == 1 && deltaY == 1 &&(y1 > y2 && whiteFigure || y1 < y2 && blackFigure);
            return result;
        }

        static bool KingFight(Cage endCage, Cage startCage)
        {
            var x1 = startCage.CagePosition[0];
            var y1 = startCage.CagePosition[1];

            var x2 = endCage.CagePosition[0];
            var y2 = endCage.CagePosition[1];

            var deltaX = Math.Abs(x1 - x2);
            var deltaY = Math.Abs(y1 - y2);

            var result = (deltaX == 1) && (deltaY == 1) || (deltaX == 0) && (deltaY == 1) || (deltaX == 1) && (deltaY == 0);
            return result;
        }
    }
}
