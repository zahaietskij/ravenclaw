using Microsoft.EntityFrameworkCore;
using NLog;
using NLog.Web;
using Ravenclaw.Backend.Hubs;
using Ravenclaw.Backend.Services;
using Ravenclaw.Backend.Services.Implementations;
using Ravenclaw.Backend.Services.Interfaces;

var logger = NLog.LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
logger.Debug("init main");
try
{
    var builder = WebApplication.CreateBuilder(args);

    // Add services to the container.
    builder.Services.AddSignalR();
    builder.Services.AddControllers();
    builder.Services.AddCors(options =>
    {
        options.AddPolicy("signalr",
            builder => builder.AllowAnyHeader()
                .AllowAnyMethod()
                .SetIsOriginAllowed((host) => true)
                .AllowCredentials());
    });
    // NLog: Setup NLog for Dependency injection
        builder.Logging.ClearProviders();
    builder.Logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
    builder.Host.UseNLog();

    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();

    builder.Services.AddDbContext<ChessDBContext>(options => options.UseInMemoryDatabase("Chess"));
    builder.Services.AddScoped<ChessDBContext>();

    builder.Services.AddSingleton<ISenderService, SenderService>();
    builder.Services.AddSingleton<IGamesService, GamesService>();
    builder.Services.AddSingleton<IQueueService, QueueService>();

    

    var app = builder.Build();

    // Configure the HTTP request pipeline.
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    //app.UseHttpsRedirection();
    
    app.UseAuthorization();
    app.UseStaticFiles();
    app.UseRouting();
    app.UseCors("signalr");
    app.UseEndpoints(endpoints =>
    {
        endpoints.MapHub<ChessHub>("/api/hubs/chess");
    });
    app.MapControllerRoute(
        name: "index",
        pattern: "{url}",
        constraints: new { url = "^/*(\\w|\\/| )*$", },
        defaults: new { controller = "Home", action = "Index" });
    app.MapGet("/", async (HttpContext context) =>
    {
        var staticIndex = File.ReadAllText("wwwroot/index.html");
        await context.Response.WriteAsync(staticIndex);
    });
    app.Run();

}
catch (Exception exception)
{
    logger.Error(exception, "Stopped program because of exception");
    throw;
}
finally
{
    // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
    NLog.LogManager.Shutdown();
}
