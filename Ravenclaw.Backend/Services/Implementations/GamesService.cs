﻿using Microsoft.EntityFrameworkCore;
using Ravenclaw.Backend.Constants;
using Ravenclaw.Backend.Models;
using System.Linq;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Ravenclaw.Backend.Services.Interfaces;

namespace Ravenclaw.Backend.Services.Implementations;

public class GamesService : IGamesService
{
    private readonly IServiceScopeFactory _scopeFactory;
    private readonly ISenderService _senderService;
    private readonly IQueueService _queueService;

    public GamesService(ISenderService senderService, IServiceScopeFactory scopeFactory, IQueueService queueService)
    {
        _scopeFactory = scopeFactory;
        _senderService = senderService;
        _queueService = queueService;
    }

    private async Task InitGame(User userA, User userB)
    {
        using var scope = _scopeFactory.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<ChessDBContext>();

        var game = new Game
        {
            GameGuid = Guid.NewGuid(),
            UserWhiteConnectionId = userA.ColorPreferred == Colors.White ? userA.ConnectionId : userB.ConnectionId,
            UserBlackConnectionId = userA.ColorPreferred == Colors.Black ? userA.ConnectionId : userB.ConnectionId,
        };
        await dbContext.ActiveGamesDbSet.AddAsync(game);
        await dbContext.SaveChangesAsync();

        await _senderService.SendMessageToUser("start_game", userA.ConnectionId,
            new { opponent = userB, color = userA.ColorPreferred, board = game.Board.GetBoard(userA.ColorPreferred) });
        await _senderService.SendMessageToUser("start_game", userB.ConnectionId,
            new { opponent = userA, color = userB.ColorPreferred, board = game.Board.GetBoard(userB.ColorPreferred) });

        await _queueService.RemoveUserFromQueue(userA);
        await _queueService.RemoveUserFromQueue(userB);
    }
    public async Task<bool> StartGame(User userA, User userB)
    {
        if (userA.ColorPreferred == Colors.Any && userB.ColorPreferred == Colors.Any)
        {
            var rnd = new Random();
            userA.ColorPreferred = (Colors)rnd.Next((int)Colors.Black, (int)Colors.White + 1);
            userB.ColorPreferred = userA.ColorPreferred == Colors.Black ? Colors.White : Colors.Black;
        }
        else if (userA.ColorPreferred == Colors.Any)
        {
            userA.ColorPreferred = userB.ColorPreferred == Colors.Black ? Colors.White : Colors.Black;
        }
        else if (userB.ColorPreferred == Colors.Any)
        {
            userB.ColorPreferred = userA.ColorPreferred == Colors.Black ? Colors.White : Colors.Black;
        }
        else if (userB.ColorPreferred == userA.ColorPreferred)
        {
            return false;
        }
        await InitGame(userA, userB);
        return true;
    }

    public async Task LeaveUserFromGame(string userConnectionId)
    {
        var game = await GetGameByUserConnectionId(userConnectionId);
        if (game != null)
        {
            await _senderService.SendMessageToUser("lost", userConnectionId, "you lost");
            if (game.UserWhiteConnectionId == userConnectionId)
            {
                await _senderService.SendMessageToUser("won", game.UserBlackConnectionId, "you won");
            }
            else
            {
                await _senderService.SendMessageToUser("won", game.UserWhiteConnectionId, "you won");
            }

            await RemoveGame(game);
        }
    }
    public async Task<bool> MakeStep(string userConnectionId, Step step)
    {
        var game = await GetGameByUserConnectionId(userConnectionId);
        if (game == null)
        {
            throw new Exception("game not found");
        }

        Colors whoColor = game.UserBlackConnectionId == userConnectionId ? Colors.Black : Colors.White;
        if (whoColor != game.CurrentMoveColor)
        {
            return false;
        }

        // validatior
        if ((Math.Abs(game.Board.Figures[8 - (step.From[0] - '0')][step.From[1] - 'a']) == FigureValues.Pawn) 
            && (whoColor == Colors.White ? step.To[0] == '8' : step.To[0] == '1'))
        {
            await MakeStep(game, step, whoColor);
            if (whoColor == Colors.White)
            {
                game.Board.Figures[8 - (step.To[0] - '0')][step.To[1] - 'a'] = step.DesiredFigure;
            }
            else
            {
                game.Board.Figures[8 - (step.To[0] - '0')][step.To[1] - 'a'] = -step.DesiredFigure;
            }
        }
        else if (((step.From[1] == 'e' && (step.To[1] == 'c' || step.To[1] == 'g')) &&  // рокіровка (треба переробити) // todo
                 (whoColor == Colors.White ? step.From[0] == '1' && step.To[0] == '1' : step.From[0] == '8' && step.To[0] == '8')) 
                 && (game.Board.Figures[8 - (step.From[0] - '0')][step.From[1] - 'a'] == FigureValues.King ))
        {
            await MakeStep(game, step, whoColor);
            await MakeStep(game, step.To[1] == 'c' ? new Step()
            {
                From = step.From[0] + "a",
                To = step.From[0] + "d"
            } : new Step()
            {
                From = step.From[0] + "h",
                To = step.From[0] + "f"
            }, whoColor);
        }
        else if (ChessValidator.ChessValidator.StepValidator(game, step))
        {
            await MakeStep(game, step, whoColor);
        }
        else
        {
            return false;
        }

        using var scope = _scopeFactory.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<ChessDBContext>();
        game.CurrentMoveColor = game.CurrentMoveColor == Colors.White ? Colors.Black : Colors.White;

        dbContext.ActiveGamesDbSet.Update(game);
        await dbContext.SaveChangesAsync();

        await ShareBoardToUsers(game);
        return true;
    }
    private async Task MakeStep(Game game, Step step, Colors whoColors)
    {
        if (whoColors == Colors.White)
        {
            if (Math.Abs(game.Board.Figures[8 - (step.To[0] - '0')][step.To[1] - 'a']) == FigureValues.King)
            {
                await LeaveUserFromGame(game.UserBlackConnectionId);
            }

            game.Board.Figures[8 - (step.To[0] - '0')][step.To[1] - 'a'] =
                game.Board.Figures[8 - (step.From[0] - '0')][step.From[1] - 'a'];

            game.Board.Figures[8 - (step.From[0] - '0')][step.From[1] - 'a'] = FigureValues.None;
        }   
        else
        {
            if (Math.Abs(game.Board.Figures[8 - (step.To[0] - '0')][step.To[1] - 'a']) == FigureValues.King)
            {
                await LeaveUserFromGame(game.UserWhiteConnectionId);
            }
            game.Board.Figures[8 - (step.To[0] - '0')][step.To[1] - 'a'] =
                game.Board.Figures[8 - (step.From[0] - '0')][step.From[1] - 'a'];

            game.Board.Figures[8 - (step.From[0] - '0')][step.From[1] - 'a'] = FigureValues.None;
        }
    }
    private async Task ShareBoardToUsers(Game game)
    {
        await _senderService.SendMessageToUser("board", game.UserBlackConnectionId, new
        {
            Board = game.Board.GetBoard(Colors.Black), CurrentStepColor = game.CurrentMoveColor
        });
        await _senderService.SendMessageToUser("board", game.UserWhiteConnectionId, new
        {
            Board =  game.Board.GetBoard(Colors.White), CurrentStepColor = game.CurrentMoveColor
        });
    }

    private async Task RemoveGame(Game game)
    {
        using var scope = _scopeFactory.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<ChessDBContext>();
        dbContext.Remove(game);
        await dbContext.SaveChangesAsync();
    }
    private async Task<Game> GetGameByUserConnectionId(string userConnectionId)
    {
        using var scope = _scopeFactory.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<ChessDBContext>();

        var game = await dbContext.ActiveGamesDbSet.SingleOrDefaultAsync(x =>
            x.UserWhiteConnectionId == userConnectionId || x.UserBlackConnectionId == userConnectionId);
        return game;
    }
}