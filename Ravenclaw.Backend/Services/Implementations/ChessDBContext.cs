﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Newtonsoft.Json;
using Ravenclaw.Backend.Models;

namespace Ravenclaw.Backend.Services.Implementations;

public class ChessDBContext : DbContext
{
    public ChessDBContext(DbContextOptions<ChessDBContext> options) : base(options) { }

    public DbSet<User> UsersQueueDbSet { get; set; }
    public DbSet<Game> ActiveGamesDbSet { get; set; }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Game>()
            .Property(x => x.Board)
            .HasConversion(new ValueConverter<Board, string>(
                v => JsonConvert.SerializeObject(v),
                v => JsonConvert.DeserializeObject<Board>(v)));
    }
}