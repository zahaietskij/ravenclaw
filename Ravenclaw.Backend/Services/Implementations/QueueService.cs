﻿using Microsoft.EntityFrameworkCore;
using Ravenclaw.Backend.Constants;
using Ravenclaw.Backend.Models;
using Ravenclaw.Backend.Services.Interfaces;

namespace Ravenclaw.Backend.Services.Implementations;

public class QueueService : IQueueService
{
    private readonly ISenderService _senderService;
    private readonly IServiceScopeFactory _scopeFactory;
    public QueueService(ISenderService senderService, IServiceScopeFactory scopeFactory)
    {
        _senderService = senderService;
        _scopeFactory = scopeFactory;
    }
    public async Task<User> AddUserToQueue(User user)
    {
        using var scope = _scopeFactory.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<ChessDBContext>();
        if (dbContext.UsersQueueDbSet.FirstOrDefault(x => x.ConnectionId == user.ConnectionId) == null)
        {
            await dbContext.UsersQueueDbSet.AddAsync(user);
            await dbContext.SaveChangesAsync();
            await _senderService.SendMessageToAllUsers("add_user_to_queue", user);
        }
        var opponent = dbContext.UsersQueueDbSet.OrderBy(x => x.EnterDateTime).FirstOrDefault(x => x.ColorPreferred != user.ColorPreferred ||
                                                                     (x.ColorPreferred == Colors.Any && x.ConnectionId != user.ConnectionId));
        return opponent;
    }
    public async Task RemoveUserFromQueue(User user)
    {
        using var scope = _scopeFactory.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<ChessDBContext>();
        if (user != null && (await dbContext.UsersQueueDbSet.ContainsAsync(user)))
        {
            dbContext.UsersQueueDbSet.Remove(user);
            await dbContext.SaveChangesAsync();
            await _senderService.SendMessageToAllUsers("remove_user_from_queue", user);
        }
    }

    public async Task<User> GetUserFromQueueByUid(Guid userUid)
    {
        using var scope = _scopeFactory.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<ChessDBContext>();
        var user = await dbContext.UsersQueueDbSet.SingleOrDefaultAsync(x => x.UserGuid == userUid);
        return user;
    }

    public async Task<User> GetUserFromQueueByConnectionId(string connectionId)
    {
        using var scope = _scopeFactory.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<ChessDBContext>();
        var user = await dbContext.UsersQueueDbSet.SingleOrDefaultAsync(x => x.ConnectionId == connectionId);
        return user;
    }

    public async Task RemoveUserFromQueue(string userConnectionId)
    {
        using var scope = _scopeFactory.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<ChessDBContext>();
        var itemToRemove = dbContext.UsersQueueDbSet.SingleOrDefault(x => x.ConnectionId == userConnectionId);
        if (itemToRemove != null)
        {
            dbContext.UsersQueueDbSet.Remove(itemToRemove);
            await dbContext.SaveChangesAsync();
            await _senderService.SendMessageToAllUsers("remove_user_from_queue", itemToRemove);
        }
    }
    public async Task<List<User>> GetQueue()
    {
        using var scope = _scopeFactory.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<ChessDBContext>();
        return await dbContext.UsersQueueDbSet.OrderBy(x => x.EnterDateTime).ToListAsync();
    }
}