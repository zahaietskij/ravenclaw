﻿using Microsoft.AspNetCore.SignalR;
using Ravenclaw.Backend.Hubs;
using Ravenclaw.Backend.Services.Interfaces;

namespace Ravenclaw.Backend.Services.Implementations;

public class SenderService : ISenderService
{
    private readonly IHubContext<ChessHub> _hubContext;

    public SenderService(IHubContext<ChessHub> hubContext)
    {
        _hubContext = hubContext;
    }

    public async Task SendMessageToUser<T>(string methodName, string userConnectionId, T arg)
    {
        await _hubContext.Clients.Client(userConnectionId).SendAsync(methodName, arg);
    }

    public async Task SendMessageToAllUsers<T>(string methodName, T arg)
    {
        await _hubContext.Clients.All.SendAsync(methodName, arg);
    }
}