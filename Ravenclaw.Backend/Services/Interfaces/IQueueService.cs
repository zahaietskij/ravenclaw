﻿using Ravenclaw.Backend.Models;

namespace Ravenclaw.Backend.Services.Interfaces;

public interface IQueueService
{
    Task<User> AddUserToQueue(User user);
    Task RemoveUserFromQueue(string userConnectionId);
    Task RemoveUserFromQueue(User user);
    Task<User> GetUserFromQueueByUid(Guid userUid);
    Task<User> GetUserFromQueueByConnectionId(string connnectionId);
    Task<List<User>> GetQueue();
}