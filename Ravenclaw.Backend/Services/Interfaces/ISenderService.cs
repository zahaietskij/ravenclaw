﻿namespace Ravenclaw.Backend.Services.Interfaces;

public interface ISenderService
{
    Task SendMessageToUser<T>(string methodName, string userConnectionId, T arg);
    Task SendMessageToAllUsers<T>(string methodName, T arg);
}