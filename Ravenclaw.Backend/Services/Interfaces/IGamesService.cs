﻿using Ravenclaw.Backend.Models;

namespace Ravenclaw.Backend.Services.Interfaces;

public interface IGamesService
{
    Task<bool> StartGame(User userA, User userB);
    Task LeaveUserFromGame(string userConnectionId);
    Task<bool> MakeStep(string userConnectionId, Step step);
}