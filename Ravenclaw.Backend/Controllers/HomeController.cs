﻿using System.Text;
using Microsoft.AspNetCore.Mvc;

namespace Ravenclaw.Backend.Controllers
{
    [ApiController]
    [Route("/")]
    public class HomeController : ControllerBase
    {
        [HttpGet]
        [Route("/{url}")]
        public IActionResult Index()
        {
            var staticIndex = System.IO.File.ReadAllText("wwwroot/index.html");
            return File(Encoding.UTF8.GetBytes(staticIndex), "text/html");
        }
    }
}
