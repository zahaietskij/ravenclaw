﻿using Microsoft.AspNetCore.SignalR;
using Ravenclaw.Backend.Constants;
using Ravenclaw.Backend.Models;
using Ravenclaw.Backend.Services.Interfaces;

namespace Ravenclaw.Backend.Hubs;

public class ChessHub : Hub
{
    private readonly IQueueService _queueService;
    private readonly ISenderService _senderService;
    private readonly IGamesService _gamesService;
    private readonly ILogger<ChessHub> _logger;

    public ChessHub(IQueueService queueService, ISenderService senderService, IGamesService gamesService, ILogger<ChessHub> logger)
    {
        _queueService = queueService;
        _senderService = senderService;
        _gamesService = gamesService;
        _logger = logger;
    }
    public override async Task OnConnectedAsync()
    {
        try
        {
            await base.OnConnectedAsync();
            var queue = await _queueService.GetQueue();
            await _senderService.SendMessageToUser("queue", Context.ConnectionId, queue);
        }
        catch (Exception e)
        {
            _logger.LogError(e, e.Message);
        }
    }
    public async Task JoinToGame(Guid opponentUid, string username)
    {
        try
        {
            var opponent = await _queueService.GetUserFromQueueByUid(opponentUid);
            var yourUser = await _queueService.GetUserFromQueueByConnectionId(Context.ConnectionId) ?? new User()
            {
                UserGuid = Guid.NewGuid(),
                ConnectionId = Context.ConnectionId,
                ColorPreferred = Colors.Any,
                Username = username
            };
            if (opponent != null)
            {
                yourUser.ColorPreferred = Colors.Any;
                var result = await _gamesService.StartGame(yourUser, opponent);
                if (!result)
                {
                    await _senderService.SendMessageToUser("error", Context.ConnectionId, "error");
                }
            }
            else
            {
                await _senderService.SendMessageToUser("error", Context.ConnectionId, "error");
            }
        }
        catch (Exception e)
        {
            _logger.LogError(e, e.Message);
        }
    }
    public async Task Step(Step step)
    {
        try
        {
            // check step
            var result = await _gamesService.MakeStep(Context.ConnectionId, step);
            if (!result)
            {
                await _senderService.SendMessageToUser("error", Context.ConnectionId, "your step is bad");
            }
        }
        catch (Exception e)
        {
            _logger.LogError(e, e.Message);
            await _senderService.SendMessageToUser("error", Context.ConnectionId, e.Message);
        }
    }

    public async Task AddUserToQueue(string username, Colors colorPreferred)
    {
        try
        {
            var user = new User()
            {
                Username = username,
                ColorPreferred = colorPreferred,
                ConnectionId = Context.ConnectionId,
                UserGuid = Guid.NewGuid(),
                EnterDateTime = DateTime.Now
            };
            var opponent = await _queueService.AddUserToQueue(user);
            if (opponent != null)
            {
                var result = await _gamesService.StartGame(user, opponent);
            }
        }
        catch (Exception e)
        {
            _logger.LogError(e, e.Message);
        }
    }

    public async Task LeaveFromQueue()
    {
        try
        {
            await _queueService.RemoveUserFromQueue(Context.ConnectionId);
        }
        catch (Exception e)
        {
            _logger.LogError(e, e.Message);
        }
    }
    public async Task LeaveFromGame()
    {
        try
        {
            await _gamesService.LeaveUserFromGame(Context.ConnectionId);
        }
        catch (Exception e)
        {
            _logger.LogError(e, e.Message);
        }
    }

    public override async Task OnDisconnectedAsync(Exception? exception)
    {
        try
        {
            await _queueService.RemoveUserFromQueue(Context.ConnectionId);
            await _gamesService.LeaveUserFromGame(Context.ConnectionId);
            await base.OnDisconnectedAsync(exception);
        }
        catch (Exception e)
        {
            _logger.LogError(e, e.Message);
        }
    }
}

