﻿using Ravenclaw.Backend.Constants;
using System.ComponentModel.DataAnnotations;

namespace Ravenclaw.Backend.Models;

public class Board
{
    [Key]
    public int Id { get; set; }
    // y x
    public int[][] Figures { get; set; }

    public Board()
    {
        this.Figures = Init();
    }
    public int[][] GetBoard(Colors whoColor)
    {
        if (whoColor == Colors.White)
        {
            var board = this.Figures.Select(x => (int[])x.Clone()).ToArray();
            SetFogOnBoard(ref board, whoColor);
            return board;
        }

        var reversedBoard = this.Figures.Select(x => (int[])x.Clone()).ToArray();
        Array.Reverse(reversedBoard);
        for (int i = 0; i < 8; i++)
        {
            Array.Reverse(reversedBoard[i]);
        }
        SetFogOnBoard(ref reversedBoard, whoColor);
        return reversedBoard;
    }
    private void SetFogOnBoard(ref int[][] board, Colors whoColor)
    {
        for (int y = 0; y < 8; y++)
        {
            for (int x = 0; x < 8; x++)
            {
                if (whoColor == Colors.White)
                {
                    if (board[y][x] <= 0)
                    {
                        board[y][x] = FigureValues.Fog;
                    }
                }
                else
                {
                    if (board[y][x] >= 0)
                    {
                        board[y][x] = FigureValues.Fog;
                    }
                }
            }
        }

        for (int y = 7; y >= 0; y--)
        {
            for (int x = 0; x < 8; x++)
            {
                OpenPartFog(ref board, x, y, whoColor);
            }
        }
    }
    public void OpenPartFog(ref int[][] board, int x, int y, Colors whoColor)
    {
        // item [x, y] and board [y, x] sorry :)
        switch (whoColor == Colors.White ? board[y][x] : -board[y][x])
        {
            case FigureValues.Pawn:
                if (y == 6)
                {
                    if (board[y - 1][x] == FigureValues.Fog)
                    {
                        board[y - 1][x] = GetOriginalFigureValue(x, y - 1, whoColor);
                        if (board[y - 1][x] != FigureValues.None)
                        {
                            if (whoColor == Colors.White && board[y - 1][x] < 0)
                            {
                                board[y - 1][x] = FigureValues.Fog;
                            }
                            else if (whoColor == Colors.Black && board[y - 1][x] > 0)
                            {
                                board[y - 1][x] = FigureValues.Fog;
                            }
                        }
                    }
                    if (board[y - 2][x] == FigureValues.Fog)
                    {
                        board[y - 2][x] = GetOriginalFigureValue(x, y - 2, whoColor);
                        if (board[y - 2][x] != FigureValues.None)
                        {
                            if (whoColor == Colors.White && board[y - 2][x] < 0)
                            {
                                board[y - 2][x] = FigureValues.Fog;
                            }
                            else if (whoColor == Colors.Black && board[y - 2][x] > 0)
                            {
                                board[y - 2][x] = FigureValues.Fog;
                            }
                        }
                    }
                }
                else
                {
                    if (CheckIndexes(x, y - 1))
                    {
                        board[y - 1][x] = GetOriginalFigureValue(x, y - 1, whoColor);
                        if (board[y - 1][x] != FigureValues.None)
                        {
                            if (whoColor == Colors.White && board[y - 1][x] < 0)
                            {
                                board[y - 1][x] = FigureValues.Fog;
                            }
                            else if (whoColor == Colors.Black && board[y - 1][x] > 0)
                            {
                                board[y - 1][x] = FigureValues.Fog;
                            }
                        }
                    }
                }
                if (CheckIndexes(x + 1, y - 1))
                {
                    if (board[y - 1][x + 1] == FigureValues.Fog)
                    {
                        board[y - 1][x + 1] = GetOriginalFigureValue(x + 1, y - 1, whoColor);
                        if (board[y - 1][x + 1] == FigureValues.None)
                        {
                            board[y - 1][x + 1] = FigureValues.Fog;
                        }
                    }
                }
                if (CheckIndexes(x - 1, y - 1))
                {
                    if (board[y - 1][x - 1] == FigureValues.Fog)
                    {
                        board[y - 1][x - 1] = GetOriginalFigureValue(x - 1, y - 1, whoColor);
                        if (board[y - 1][x - 1] == FigureValues.None)
                        {
                            board[y - 1][x - 1] = FigureValues.Fog;
                        }
                    }
                }
                break;
            case FigureValues.Knight:
                OpenWay(ref board, x, y, whoColor, FigureValues.KnightSteps, true);
                break;
            case FigureValues.Bishop:
                OpenWay(ref board, x, y, whoColor, FigureValues.TopLeftSteps, false);
                OpenWay(ref board, x, y, whoColor, FigureValues.TopRightSteps, false);
                OpenWay(ref board, x, y, whoColor, FigureValues.DownLeftSteps, false);
                OpenWay(ref board, x, y, whoColor, FigureValues.DownRightSteps, false);
                break;
            case FigureValues.Rook:
                OpenWay(ref board, x, y, whoColor, FigureValues.TopSteps, false);
                OpenWay(ref board, x, y, whoColor, FigureValues.DownSteps, false);
                OpenWay(ref board, x, y, whoColor, FigureValues.RightSteps, false);
                OpenWay(ref board, x, y, whoColor, FigureValues.LeftSteps, false);
                break;
            case FigureValues.Queen:
                OpenWay(ref board, x, y, whoColor, FigureValues.TopSteps, false);
                OpenWay(ref board, x, y, whoColor, FigureValues.DownSteps, false);
                OpenWay(ref board, x, y, whoColor, FigureValues.RightSteps, false);
                OpenWay(ref board, x, y, whoColor, FigureValues.LeftSteps, false);
                OpenWay(ref board, x, y, whoColor, FigureValues.TopLeftSteps, false);
                OpenWay(ref board, x, y, whoColor, FigureValues.TopRightSteps, false);
                OpenWay(ref board, x, y, whoColor, FigureValues.DownLeftSteps, false);
                OpenWay(ref board, x, y, whoColor, FigureValues.DownRightSteps, false);
                break;
            case FigureValues.King:
                OpenWay(ref board, x, y, whoColor, FigureValues.KingSteps, true);
                break;
            default:
                break;
        }
    }

    private void OpenWay(ref int[][] board, int x, int y, Colors whoColor, List<int[]> ways, bool isSkip)
    {
        foreach (var item in ways.Where(item => CheckIndexes(x + item[0], y + item[1])))
        {
            board[y + item[1]][x + item[0]] = GetOriginalFigureValue(x + item[0], y + item[1], whoColor);
            if (!isSkip)
            {
                if (Math.Abs(board[y + item[1]][x + item[0]]) > 1)
                {
                    return;
                }
            }
        }
    }
    private int GetOriginalFigureValue(int x, int y, Colors whoColor)
    {
        if (whoColor == Colors.White)
        {
            return this.Figures[y][x];
        }

        return this.Figures[7 - y][7 - x];
    }
    private static bool CheckIndexes(int x, int y) =>
        x is >= 0 and < 8 && y is >= 0 and < 8;
    private static int[][] Init()
    {
        return new int[][] { // black
            new int[] { -FigureValues.Rook, -FigureValues.Knight, -FigureValues.Bishop, -FigureValues.Queen, -FigureValues.King, -FigureValues.Bishop, -FigureValues.Knight, -FigureValues.Rook },
            new int[] { -FigureValues.Pawn, -FigureValues.Pawn, -FigureValues.Pawn, -FigureValues.Pawn, -FigureValues.Pawn, -FigureValues.Pawn, -FigureValues.Pawn, -FigureValues.Pawn },
            new int[] { FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None },
            new int[] { FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None },
            new int[] { FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None },
            new int[] { FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None, FigureValues.None },
            new int[] { +FigureValues.Pawn, +FigureValues.Pawn, +FigureValues.Pawn, +FigureValues.Pawn, +FigureValues.Pawn, +FigureValues.Pawn, +FigureValues.Pawn, +FigureValues.Pawn },
            new int[] { +FigureValues.Rook, +FigureValues.Knight, +FigureValues.Bishop, +FigureValues.Queen, +FigureValues.King, +FigureValues.Bishop, +FigureValues.Knight, +FigureValues.Rook },
        }; // white
    }
}