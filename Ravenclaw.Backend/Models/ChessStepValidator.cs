﻿namespace Ravenclaw.Backend.Models;

public class ChessStepValidator
{
    public int[] From { get; set; }
    public int[] To { get; set; }
}