﻿using System.ComponentModel.DataAnnotations;
using Ravenclaw.Backend.Constants;

namespace Ravenclaw.Backend.Models;

public class User
{
    [Key]
    public Guid UserGuid { get; set; }
    public string ConnectionId { get; set; }  
    public string Username { get; set; }
    public Colors ColorPreferred { get; set; }
    public DateTime EnterDateTime { get; set; }
}