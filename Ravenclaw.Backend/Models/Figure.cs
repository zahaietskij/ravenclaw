﻿using Ravenclaw.Backend.Constants;

namespace Ravenclaw.Backend.Models;

public class Figure
{
    public Figure(Colors color, int value)
    {
        this.Color = color;
        this.Value = value;
    }
    public Colors Color { get; set; }
    public int Value { get; set; }
}