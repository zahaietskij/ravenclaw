﻿using Ravenclaw.Backend.Constants;

namespace Ravenclaw.Backend.Models
{
    
        public class Cage
        {
            public int Figure { get; set; }
            public int[] CagePosition { get; set; } = { 0, 0 };
        }

}
