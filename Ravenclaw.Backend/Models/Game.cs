﻿using Ravenclaw.Backend.Constants;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ravenclaw.Backend.Models;

public class Game
{
    [Key]
    public Guid GameGuid { get; set; }
    public string UserWhiteConnectionId { get; set; }
    public string UserBlackConnectionId { get; set; }
    public Colors CurrentMoveColor { get; set; } = Colors.White;
    public Board Board { get; set; } = new Board();
}