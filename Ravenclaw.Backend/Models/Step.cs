﻿namespace Ravenclaw.Backend.Models;

public class Step
{
    public string From { get; set; }
    public string To { get; set; }
    public int DesiredFigure { get; set; }
}
