# ravenclaw

chess

### Instructions to run an angular project:

Step 1) Go to the catalog of your project: `cd Ravenclaw.Frontend`

Step 2) Install packages: `npm install`

Step 3) Start the dev server: `ng serve`

### Instructions to run an .net project:

Step 1) Go to the catalog of your project: `cd Ravenclaw`

Step 2) Run: `docker-compose -f docker-compose.prod.yml up -d`
