import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { GameService } from './game.service';

@Injectable()
export class GameGuard implements CanActivate {
  constructor(
    private gameService: GameService,
    private router: Router,
  ) { }
  canActivate(): boolean | UrlTree {
    if (this.gameService.isGame()) {
      return true;
    }
    return this.router.createUrlTree(['/']);
  }
}