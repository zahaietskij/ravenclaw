import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { SignalRService } from './signalr.service';

@Injectable()
export class UserQueueService {

  private _userList: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  public userList$: Observable<any[]> = this._userList.asObservable();

  constructor(
    private signalRService: SignalRService,
  ) {

    this.signalRService.queue$.subscribe(queue => {
      this._userList.next(queue);
    });

    this.signalRService.addedUser$.subscribe(addedUser => {
      if (addedUser) {
        const users = this._userList.value;
        this._userList.next([...users, addedUser]);
      }
    });

    this.signalRService.removedUser$.subscribe(removedUser => {
      if (removedUser) {
        const users = this._userList.value.filter(user => user.userGuid !== removedUser.userGuid);
        this._userList.next([...users]);
      }
    });
  }

  public addUser(nickname: string, color: number) {
    if (nickname && color) {
      this.signalRService.addUserToQueue(nickname, color);
    }
  }

  public leaveFromQueue() {
    this.signalRService.leaveFromQueue();
  }

  public joinToGame(opponentUid: string, nickname: string) {
    if (opponentUid && nickname) {
      this.signalRService.joinToGame(opponentUid, nickname);
    }
  }
}