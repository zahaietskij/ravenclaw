import { Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class SignalRService {

  private hubConnection: HubConnection;

  private _queue = new BehaviorSubject<any[]>([]);
  public queue$: Observable<any[]> = this._queue.asObservable();

  private _addedUser = new BehaviorSubject<any>(null);
  public addedUser$: Observable<any> = this._addedUser.asObservable();

  private _removedUser = new BehaviorSubject<any>(null);
  public removedUser$: Observable<any> = this._removedUser.asObservable();

  private _startGame = new BehaviorSubject<any>(null);
  public startGame$: Observable<any> = this._startGame.asObservable();

  private _userWon = new BehaviorSubject<any>(null);
  public userWon$: Observable<any> = this._userWon.asObservable();

  private _userLost = new BehaviorSubject<any>(null);
  public userLost$: Observable<any> = this._userLost.asObservable();

  private _serverBoard: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public serverBoard$: Observable<any> = this._serverBoard.asObservable();

  constructor() {
    this.hubConnection = new HubConnectionBuilder()
      .withUrl(environment.apiChessHubUrl)
      .build();

    this.hubConnection.on('queue', (data) => {
      if (data.length) {
        this._queue.next(data);
      }
    });

    this.hubConnection.on('add_user_to_queue', (data) => {
      if (data) {
        this._addedUser.next({
          ...data,
          isYou: data.connectionId === this.hubConnection.connectionId,
        });
      }
    });

    this.hubConnection.on('remove_user_from_queue', (data) => {
      if (data) {
        this._removedUser.next(data);
      }
    });

    this.hubConnection.on('start_game', (data) => {
      if (data) {
        this._startGame.next(data);
      }
    });

    this.hubConnection.on('won', (data) => {
      if (data) {
        this._userWon.next(data);
      }
    });

    this.hubConnection.on('lost', (data) => {
      if (data) {
        this._userLost.next(data);
      }
    });

    this.hubConnection.on('board', (data) => {
      if (data) {
        this._serverBoard.next(data);
      }
    });
  }

  public addUserToQueue(nickname: string, color: number) {
    this.hubConnection.invoke('AddUserToQueue', nickname, color);
  }

  public leaveFromQueue() {
    this.hubConnection.invoke('LeaveFromQueue');
  }

  public joinToGame(opponentUid: string, nickname: string) {
    this.hubConnection.invoke('JoinToGame', opponentUid, nickname);
  }

  public step(step: object) {
    this.hubConnection.invoke('Step', step);
  }

  public leaveFromGame() {
    this.hubConnection.invoke('LeaveFromGame');
  }

  public connect() {
    this.hubConnection.start()
      .then(() => {
        console.log('Connection started');
      })
      .catch((err) => console.log('Error while establishing signalr connection: ' + err));
  }
}