import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { SignalRService } from './signalr.service';

@Injectable()
export class GameService {

  private _chessBoard: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  public chessBoard$: Observable<any[]> = this._chessBoard.asObservable();

  private _username: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public username$: Observable<string> = this._username.asObservable();

  private _currentMove: BehaviorSubject<string> = new BehaviorSubject<string>('White');
  public currentMove$: Observable<string> = this._currentMove.asObservable();

  public onGameStart: EventEmitter<boolean> = new EventEmitter<boolean>(false);

  public gameResult: EventEmitter<string> = new EventEmitter<string>();

  public userColor!: string;
  public opponent = {
    colorPreferred: '',
    username: '',
  };

  private whiteKingCount = 0;
  private blackKingCount = 0;
  private whiteLeftRook = 0;
  private whiteRightRook = 0;
  private blackLeftRook = 0;
  private blackRightRook = 0;

  isLoggedIn = false;

  constructor(
    private signalRService: SignalRService,
  ) {

    this.signalRService.startGame$.subscribe(startGame => {
      if (startGame && startGame.board && startGame.color && startGame.opponent) {
        this.userColor = this.convertUserColor(startGame.color);
        this.opponent.username = startGame.opponent.username;
        this.opponent.colorPreferred = this.convertUserColor(startGame.opponent.colorPreferred);
        this.onGameStart.emit(true);
        this._chessBoard.next(this.convertBoard(startGame.board));
      }
    });

    this.signalRService.userWon$.subscribe(data => {
      if (data) {
        this.gameResult.emit('won');
      }
    });

    this.signalRService.userLost$.subscribe(data => {
      if (data) {
        this.gameResult.emit('lost');
      }
    });

    this.signalRService.serverBoard$.subscribe(data => {
      if (data) {
        this._chessBoard.next(this.convertBoard(data.board));
        this._currentMove.next(this.convertUserColor(data.currentStepColor));

      }
    });
  }

  public step(step: any) {
    if (step) {
      this.signalRService.step(step);
      if (this.userColor === 'White') {
        if (step.from === '1e') {
          this.whiteKingCount++;
        }
        if (step.from === '1a') {
          this.whiteLeftRook++;
        }
        if (step.from === '1h') {
          this.whiteRightRook++;
        }
      }
      if (this.userColor === 'Black') {
        if (step.from === '8e') {
          this.blackKingCount++;
        }
        if (step.from === '8a') {
          this.blackLeftRook++;
        }
        if (step.from === '8h') {
          this.blackRightRook++;
        }
      }
    }
  }

  public leaveFromGame() {
    this.signalRService.leaveFromGame();
  }

  isGame() {
    return this.isLoggedIn;
  }

  logIn() {
    this.isLoggedIn = true;
  }

  setUsername(data: string) {
    this._username.next(data);
  }

  convertBoard(chessBoard: any[]) {
    let newBoard: Array<any> = [];
    for (let i = 0; i < chessBoard.length; i++) {
      newBoard[i] = [];
      for (let j = 0; j < chessBoard[i].length; j++) {
        let cell: any = {};
        if (chessBoard[i][j] < 0) {
          cell.color = 'Black';
        } else if (chessBoard[i][j] > 0) {
          cell.color = 'White';
        } else {
          cell.color = 'None';
        }

        switch (Math.abs(chessBoard[i][j])) {
          case 1:
            cell.value = 'Fog';
            cell.color = 'None';
            break;
          case 5:
            cell.value = 'Pawn';
            break;
          case 7:
            cell.value = 'Knight';
            break;
          case 9:
            cell.value = 'Bishop';
            break;
          case 10:
            cell.value = 'Rook';
            break;
          case 50:
            cell.value = 'Queen';
            break;
          case 100:
            cell.value = 'King';
            break;
          default:
            cell.value = 'None';
            break;
        }
        newBoard[i][j] = cell;
      }
    }
    return newBoard;
  }

  convertUserColor(userColor: number): string {
    let newUserColor = '';
    if (userColor === 1) {
      newUserColor = 'Black';
    } else {
      newUserColor = 'White';
    }
    return newUserColor;
  }

  numberToNotation(line: number, cell: number): string {
    let column = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
    let newLine, newCell;
    if (this.userColor === 'Black') {
      newLine = line + 1;
      newCell = column[7 - cell];
    } else {
      newLine = 8 - line;
      newCell = column[cell];
    }
    return newLine + newCell;
  }

  figureDefinition(line: number, cell: number): Array<any> {
    let validation: Array<any> = [];
    switch (this._chessBoard.value[line][cell].value) {
      case 'Pawn':
        validation = this.pawnValidation(line, cell);
        break;
      case 'Knight':
        validation = this.knightValidation(line, cell);
        break;
      case 'Bishop':
        validation = this.bishopValidation(line, cell);
        break;
      case 'Rook':
        validation = this.rookValidation(line, cell);
        break;
      case 'Queen':
        validation = this.queenValidation(line, cell);
        break;
      case 'King':
        validation = this.kingValidation(line, cell);
        break;
    }
    return validation;
  }

  bishopValidation(line: number, cell: number): Array<any> {
    let possibleMoves = [];
    let newLine, newCell, moveType;
    for (let x = 1; x < 8; x++) {
      newLine = line + x;
      newCell = cell - x;
      moveType = this.getMoveType(newLine, newCell);
      if (moveType != 'unnamed') {
        possibleMoves.push({ line: newLine, cell: newCell });
      }
      if (moveType != 'empty') {
        break;
      }
    }

    for (let x = 1; x < 8; x++) {
      newLine = line + x;
      newCell = cell + x;
      moveType = this.getMoveType(newLine, newCell);
      if (moveType != 'unnamed') {
        possibleMoves.push({ line: newLine, cell: newCell });
      }
      if (moveType != 'empty') {
        break;
      }
    }

    for (let x = 1; x < 8; x++) {
      newLine = line - x;
      newCell = cell - x;
      moveType = this.getMoveType(newLine, newCell);
      if (moveType != 'unnamed') {
        possibleMoves.push({ line: newLine, cell: newCell });
      }
      if (moveType != 'empty') {
        break;
      }
    }

    for (let x = 1; x < 8; x++) {
      newLine = line - x;
      newCell = cell + x;
      moveType = this.getMoveType(newLine, newCell);
      if (moveType != 'unnamed') {
        possibleMoves.push({ line: newLine, cell: newCell });
      }
      if (moveType != 'empty') {
        break;
      }
    }
    return possibleMoves;
  }

  rookValidation(line: number, cell: number): Array<any> {
    let possibleMoves = [];
    let newLine, newCell, moveType;
    for (let x = 1; x < 8; x++) {
      newLine = line + x;
      moveType = this.getMoveType(newLine, cell);
      if (moveType != 'unnamed') {
        possibleMoves.push({ line: newLine, cell: cell });
      }
      if (moveType != 'empty') {
        break;
      }
    }

    for (let x = 1; x < 8; x++) {
      newLine = line - x;
      moveType = this.getMoveType(newLine, cell);
      if (moveType != 'unnamed') {
        possibleMoves.push({ line: newLine, cell: cell });
      }
      if (moveType != 'empty') {
        break;
      }
    }

    for (let x = 1; x < 8; x++) {
      newCell = cell + x;
      moveType = this.getMoveType(line, newCell);
      if (moveType != 'unnamed') {
        possibleMoves.push({ line: line, cell: newCell });
      }
      if (moveType != 'empty') {
        break;
      }
    }

    for (let x = 1; x < 8; x++) {
      newCell = cell - x;
      moveType = this.getMoveType(line, newCell);
      if (moveType != 'unnamed') {
        possibleMoves.push({ line: line, cell: newCell });
      }
      if (moveType != 'empty') {
        break;
      }
    }
    return possibleMoves;
  }

  knightValidation(line: number, cell: number): Array<any> {
    let possibleMoves = [];
    let moveType;
    const movesToCheck = [
      {
        newLine: line + 2,
        newCell: cell + 1,
      },
      {
        newLine: line + 2,
        newCell: cell - 1,
      },
      {
        newLine: line - 2,
        newCell: cell + 1,
      },
      {
        newLine: line - 2,
        newCell: cell - 1,
      },
      {
        newLine: line + 1,
        newCell: cell - 2,
      },
      {
        newLine: line - 1,
        newCell: cell - 2,
      },
      {
        newLine: line + 1,
        newCell: cell + 2,
      },
      {
        newLine: line - 1,
        newCell: cell + 2,
      },
    ];

    for (let i = 0; i < movesToCheck.length; i++) {
      moveType = this.getMoveType(movesToCheck[i].newLine, movesToCheck[i].newCell);
      if (moveType != 'unnamed') {
        possibleMoves.push({ line: movesToCheck[i].newLine, cell: movesToCheck[i].newCell });
      }
    }
    return possibleMoves;
  }

  queenValidation(line: number, cell: number): Array<any> {
    let possibleMoves = [];
    let newLine, newCell, moveType;
    for (let x = 1; x < 8; x++) {
      newLine = line + x;
      moveType = this.getMoveType(newLine, cell);
      if (moveType != 'unnamed') {
        possibleMoves.push({ line: newLine, cell: cell });
      }
      if (moveType != 'empty') {
        break;
      }
    }

    for (let x = 1; x < 8; x++) {
      newLine = line - x;
      moveType = this.getMoveType(newLine, cell);
      if (moveType != 'unnamed') {
        possibleMoves.push({ line: newLine, cell: cell });
      }
      if (moveType != 'empty') {
        break;
      }
    }

    for (let x = 1; x < 8; x++) {
      newCell = cell + x;
      moveType = this.getMoveType(line, newCell);
      if (moveType != 'unnamed') {
        possibleMoves.push({ line: line, cell: newCell });
      }
      if (moveType != 'empty') {
        break;
      }
    }

    for (let x = 1; x < 8; x++) {
      newCell = cell - x;
      moveType = this.getMoveType(line, newCell);
      if (moveType != 'unnamed') {
        possibleMoves.push({ line: line, cell: newCell });
      }
      if (moveType != 'empty') {
        break;
      }
    }

    for (let x = 1; x < 8; x++) {
      newLine = line + x;
      newCell = cell - x;
      moveType = this.getMoveType(newLine, newCell);
      if (moveType != 'unnamed') {
        possibleMoves.push({ line: newLine, cell: newCell });
      }
      if (moveType != 'empty') {
        break;
      }
    }

    for (let x = 1; x < 8; x++) {
      newLine = line + x;
      newCell = cell + x;
      moveType = this.getMoveType(newLine, newCell);
      if (moveType != 'unnamed') {
        possibleMoves.push({ line: newLine, cell: newCell });
      }
      if (moveType != 'empty') {
        break;
      }
    }

    for (let x = 1; x < 8; x++) {
      newLine = line - x;
      newCell = cell - x;
      moveType = this.getMoveType(newLine, newCell);
      if (moveType != 'unnamed') {
        possibleMoves.push({ line: newLine, cell: newCell });
      }
      if (moveType != 'empty') {
        break;
      }
    }

    for (let x = 1; x < 8; x++) {
      newLine = line - x;
      newCell = cell + x;
      moveType = this.getMoveType(newLine, newCell);
      if (moveType != 'unnamed') {
        possibleMoves.push({ line: newLine, cell: newCell });
      }
      if (moveType != 'empty') {
        break;
      }
    }
    return possibleMoves;
  }

  pawnValidation(line: number, cell: number): Array<any> {
    let possibleMoves = [];
    let newLine, moveType;
    newLine = line - 2;
    if (newLine === 4 && this._chessBoard.value[5][cell].value === 'None') {
      moveType = this.getMoveType(newLine, cell);
      if (moveType === 'empty') {
        possibleMoves.push({ line: newLine, cell: cell });
      }
    }
    newLine = line - 1;
    moveType = this.getMoveType(newLine, cell);
    if (moveType === 'empty') {
      possibleMoves.push({ line: newLine, cell: cell });
    }
    if (line - 1 >= 0 && line - 1 <= 7 && cell + 1 >= 0 && cell + 1 <= 7 &&
      this._chessBoard.value[line - 1][cell + 1].color === this.opponent.colorPreferred) {
      possibleMoves.push({ line: line - 1, cell: cell + 1 });
    }
    if (line - 1 >= 0 && line - 1 <= 7 && cell - 1 >= 0 && cell - 1 <= 7 &&
      this._chessBoard.value[line - 1][cell - 1].color === this.opponent.colorPreferred) {
      possibleMoves.push({ line: line - 1, cell: cell - 1 });
    }
    return possibleMoves;
  }

  kingValidation(line: number, cell: number): Array<any> {
    let possibleMoves = [];
    let moveType;
    const movesToCheck = [
      {
        newLine: line - 1,
        newCell: cell - 1,
      },
      {
        newLine: line - 1,
        newCell: cell,
      },
      {
        newLine: line - 1,
        newCell: cell + 1,
      },
      {
        newLine: line,
        newCell: cell - 1,
      },
      {
        newLine: line,
        newCell: cell + 1,
      },
      {
        newLine: line + 1,
        newCell: cell - 1,
      },
      {
        newLine: line + 1,
        newCell: cell + 1,
      },
      {
        newLine: line + 1,
        newCell: cell,
      },
    ];

    for (let i = 0; i < movesToCheck.length; i++) {
      moveType = this.getMoveType(movesToCheck[i].newLine, movesToCheck[i].newCell);
      if (moveType != 'unnamed') {
        possibleMoves.push({ line: movesToCheck[i].newLine, cell: movesToCheck[i].newCell });
      }
    }

    if (this.userColor === 'White') {
      if (this.whiteKingCount === 0 && this.whiteLeftRook === 0 &&
        this._chessBoard.value[7][1].value === 'None' &&
        this._chessBoard.value[7][2].value === 'None' &&
        this._chessBoard.value[7][3].value === 'None') {
        possibleMoves.push({ line: 7, cell: 2 });
      }
      if (this.whiteKingCount === 0 && this.whiteRightRook === 0 &&
        this._chessBoard.value[7][5].value === 'None' &&
        this._chessBoard.value[7][6].value === 'None') {
        possibleMoves.push({ line: 7, cell: 6 });
      }
    }

    if (this.userColor === 'Black') {
      if (this.blackKingCount === 0 && this.blackLeftRook === 0 &&
        this._chessBoard.value[7][4].value === 'None' &&
        this._chessBoard.value[7][5].value === 'None' &&
        this._chessBoard.value[7][6].value === 'None') {
        possibleMoves.push({ line: 7, cell: 5 });
      }
      if (this.blackKingCount === 0 && this.blackRightRook === 0 &&
        this._chessBoard.value[7][1].value === 'None' &&
        this._chessBoard.value[7][2].value === 'None') {
        possibleMoves.push({ line: 7, cell: 1 });
      }
    }
    return possibleMoves;
  }

  getMoveType(line: number, cell: number): string {
    if (line >= 0 && line <= 7 && cell >= 0 && cell <= 7 &&
      this._chessBoard.value[line][cell].value === 'None') {
      return 'empty';
    } else if (line >= 0 && line <= 7 && cell >= 0 && cell <= 7 &&
      this._chessBoard.value[line][cell].color === this.opponent.colorPreferred) {
      return 'enemy';
    }
    return 'unnamed';
  }
}