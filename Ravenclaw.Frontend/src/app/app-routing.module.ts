import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameComponent } from './components/game/game.component';
import { StartGameComponent } from './components/start-game/start-game.component';
import { GameGuard } from './services/game-guard.service';

const routes: Routes = [
  { path: '', component: StartGameComponent },
  { path: 'game', component: GameComponent, canActivate: [GameGuard] },
  { path: '**', component: StartGameComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
