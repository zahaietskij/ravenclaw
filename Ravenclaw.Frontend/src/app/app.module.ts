import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StartGameComponent } from './components/start-game/start-game.component';
import { RegistrationFormComponent } from './components/registration-form/registration-form.component';

import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatRadioModule } from '@angular/material/radio';
import { SignalRService } from './services/signalr.service';
import { GameComponent } from './components/game/game.component';
import { UserQueueService } from './services/user-queue.service';
import { GameService } from './services/game.service';
import { ResultComponent } from './components/result/result.component';
import { ChoiceFigureComponent } from './components/choice-figure/choice-figure.component';
import { GameGuard } from './services/game-guard.service';



@NgModule({
  declarations: [
    AppComponent,
    StartGameComponent,
    RegistrationFormComponent,
    GameComponent,
    ResultComponent,
    ChoiceFigureComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatButtonModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    BrowserAnimationsModule,
    MatRadioModule,
    FormsModule,
  ],
  providers: [
    SignalRService,
    UserQueueService,
    GameService,
    GameGuard,
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
