import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from './services/game.service';
import { SignalRService } from './services/signalr.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  constructor(
    private signalRService: SignalRService,
    private gameService: GameService,
    private router: Router,
  ) {
    this.signalRService.connect();

    this.gameService.onGameStart.subscribe(start => {
      if (start) {
        this.router.navigate(['/game']);
      }
    });
  }
}
