import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { GameService } from 'src/app/services/game.service';
import { ChoiceFigureComponent } from '../choice-figure/choice-figure.component';
import { ResultComponent } from '../result/result.component';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css'],
})
export class GameComponent {

  figurePosition = {
    line: -1,
    cell: -1,
  };

  step = {
    from: '',
    to: '',
    desiredFigure: 0,
  };

  opponent = this.gameService.opponent;

  user = {
    username: '',
    color: this.gameService.userColor,
  };

  board!: any[];

  posibleMoves: Array<any> = [];

  currentMove = '';

  letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private gameService: GameService,
  ) {

    this.gameService.gameResult.subscribe(result => {
      if (result) {
        this.openDialog(result);
      }
    });
    this.gameService.chessBoard$.subscribe(chessBoard => {
      this.board = chessBoard;
    });
    this.gameService.username$.subscribe(data => {
      this.user.username = data;
    });
    this.gameService.currentMove$.subscribe(currentMove => {
      this.currentMove = currentMove;
    });

  }

  openDialog(result: string): void {
    const dialogRef = this.dialog.open(ResultComponent, {
      height: '200px',
      width: '400px',
      disableClose: true,
      data: {
        result,
      },
    });
    dialogRef.afterClosed().subscribe(() => {
      this.router.navigate(['/']);
    });
  }

  openFigureChoiceDialog(): void {
    const dialogRef = this.dialog.open(ChoiceFigureComponent, {
      height: '200px',
      width: '400px',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.step.desiredFigure = result;
        this.gameService.step(this.step);
      }
    });
  }


  getPosition(line: number, cell: number) {
    if (this.board[line][cell].color === this.gameService.userColor) {
      this.figurePosition.line = line;
      this.figurePosition.cell = cell;
      this.posibleMoves = this.gameService.figureDefinition(line, cell);
      this.step.from = this.gameService.numberToNotation(this.figurePosition.line, this.figurePosition.cell);
    } else {
      const prevLine = this.figurePosition.line;
      const prevCell = this.figurePosition.cell;
      if (prevLine != -1 && prevCell != -1 &&
        this.board[prevLine][prevCell].color === this.gameService.userColor &&
        this.currentMove === this.gameService.userColor) {
        for (let i = 0; i < this.posibleMoves.length; i++) {
          if (this.posibleMoves[i].line === line && this.posibleMoves[i].cell === cell) {
            this.figurePosition.line = -1;
            this.figurePosition.cell = -1;
            this.step.to = this.gameService.numberToNotation(line, cell);
            console.log(line);
            if (this.board[prevLine][prevCell].value === 'Pawn' && line === 0) {
              this.openFigureChoiceDialog();
            } else {
              this.gameService.step(this.step);
            }
            this.posibleMoves = [];
          }
        }
      }
    }
  }

  getImageSrc(cell: any): string {
    return '/assets/' + cell.color + '_' + cell.value + '.svg';
  }

  getImageClasses(cell: any): Array<string> {
    const classes = [];

    if (cell.value === 'Fog') {
      classes.push('fog');
    }

    if (cell.value !== 'Fog' && cell.value !== 'None') {
      classes.push('figure');
    }
    return classes;
  }

  getCellClasses(line: number, cell: number): Array<string> {
    const classes = [];
    if (this.figurePosition?.line === line && this.figurePosition?.cell === cell) {
      classes.push('select');
    } else if (this.posibleMoves.findIndex(move => move.line === line && move.cell === cell) != -1) {
      if (this.board[line][cell].color === this.gameService.opponent.colorPreferred) {
        classes.push('fight');
      } else {
        classes.push('move');
      }
    }
    return classes;
  }

  surrender() {
    this.gameService.leaveFromGame();
  }
}
