import { Component, HostListener } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-choice-figure',
  templateUrl: './choice-figure.component.html',
  styleUrls: ['./choice-figure.component.css'],
})
export class ChoiceFigureComponent {
  constructor(
    public dialogRef: MatDialogRef<ChoiceFigureComponent>,
  ) {}

  @HostListener('window:keyup.Enter', ['$event'])
  onDialogClick(): void {
    this.save();
  }

  figure = 50;

  save() {
    this.dialogRef.close(this.figure);
  }

}
