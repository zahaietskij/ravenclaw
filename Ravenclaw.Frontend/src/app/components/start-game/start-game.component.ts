import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GameService } from 'src/app/services/game.service';
import { UserQueueService } from 'src/app/services/user-queue.service';
import { RegistrationFormComponent } from '../registration-form/registration-form.component';

@Component({
  selector: 'app-start-game',
  templateUrl: './start-game.component.html',
  styleUrls: ['./start-game.component.css'],
})
export class StartGameComponent {
  nickname!: string;

  color!: number;

  userList: any[] = [];


  constructor(
    public dialog: MatDialog,
    private userQueueService: UserQueueService,
    private gameService: GameService,
  ) {
    this.userQueueService.userList$.subscribe((users) => {
      this.userList = users;
    });

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(RegistrationFormComponent, {
      height: '300px',
      width: '500px',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.nickname && result.color) {
        this.nickname = result.nickname;
        this.color = result.color;
        this.gameService.setUsername(this.nickname);
        this.userQueueService.addUser(this.nickname, this.color);
        this.gameService.logIn();
      }
    });
  }

  deleteUser() {
    this.userQueueService.leaveFromQueue();
    this.nickname = '';
    this.color = 0;
  }

  joinToGame(userGuid: string) {
    if (this.nickname) {
      this.userQueueService.joinToGame(userGuid, this.nickname);
    } else {
      const dialogRef = this.dialog.open(RegistrationFormComponent, {
        height: '200px',
        width: '400px',
        disableClose: true,
        data: { hideColor: true },
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result.nickname) {
          this.nickname = result.nickname;
          this.gameService.setUsername(this.nickname);
          this.userQueueService.joinToGame(userGuid, this.nickname);
          this.gameService.logIn();
        }
      });
    }
 
  }
}
