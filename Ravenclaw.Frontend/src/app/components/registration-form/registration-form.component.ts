import { Component, HostListener, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css'],
})

export class RegistrationFormComponent {
  constructor(
    public dialogRef: MatDialogRef<RegistrationFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
  ) {}

  @HostListener('window:keyup.Enter', ['$event'])
  onDialogClick(): void {
    this.save();
  }

  user = {
    nickname: '',
    color: 3,
  };

  save() {
    this.dialogRef.close(this.user);
  }

}
